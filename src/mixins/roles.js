export default {
    props: ['roles'],
    data(){
        return {
            members: [],
            dataDef: {
                roles: {placeholder: 'no role', default: 'user', type: 'select', label: 'Select role for user'}
            }
        }
    },

}